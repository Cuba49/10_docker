FROM node:6-alpine

COPY server ./server
COPY package*.json ./
RUN npm install

CMD ["node", "server/index.js"]
EXPOSE $PORT
