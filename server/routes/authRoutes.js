const { Router } = require("express");
const AuthService = require("../services/authService");

const router = Router();

router.post("/login", (req, res, next) => {
	try {
		// TODO: Implement login action (get the user if it exist with entered credentials)
		const data = AuthService.login(
			(elem) =>
				elem.email === req.body.email && elem.password === req.body.password
		);
		res.data = data;
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
});

module.exports = router;
