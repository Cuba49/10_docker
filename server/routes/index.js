const userRoutes = require("./userRoutes");
const authRoutes = require("./authRoutes");
const messageRoutes = require("./messageRoutes");
const { responseMiddleware } = require("../middlewares/response.middleware");

module.exports = (app) => {
	app.use("/api/users", userRoutes);
	app.use("/api/messages", messageRoutes);
	app.use("/api/auth", authRoutes);
	app.use(responseMiddleware);
};
