const { Router } = require("express");
const MessageService = require("../services/messageService");

const router = Router();

router.get("/", (req, res, next) => {
	try {
		const data = MessageService.getAll();
		res.data = data;
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
});

router.get("/:id", (req, res, next) => {
	try {
		const data = MessageService.search((elem) => elem.id === req.params.id);
		res.data = data;
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
});

router.post("/", (req, res, next) => {
	try {
		if (!res.err) {
			const data = MessageService.create(req.body);
			res.data = data;
		}
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
});

router.put("/:id", (req, res, next) => {
	try {
		if (!res.err) {
			const data = MessageService.update(req.params.id, req.body);
			res.data = data;
		}
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
});

router.delete("/:id", (req, res, next) => {
	try {
		const data = MessageService.delete(req.params.id);
		res.data = data;
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
});

module.exports = router;
