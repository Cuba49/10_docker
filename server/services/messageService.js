const { MessageRepository } = require("../repositories/messageRepository");

class MessageService {
	getAll() {
		const items = MessageRepository.getAll();
		if (!items) {
			return null;
		}
		return items.sort(
			(a, b) => new Date(b.createdAt) - new Date(a.createdAt)
		);
	}

	search(search) {
		const item = MessageRepository.getOne(search);
		if (!item) {
			return null;
		}
		return item;
	}

	create(data) {
		const item = MessageRepository.create(data);
		if (!item) {
			return null;
		}
		return item;
	}

	update(id, data) {
		const item = MessageRepository.update(id, data);
		if (!item) {
			return null;
		}
		return item;
	}

	delete(id) {
		const item = MessageRepository.delete(id);
		if (!item) {
			return null;
		}
		return item;
	}
}

module.exports = new MessageService();
