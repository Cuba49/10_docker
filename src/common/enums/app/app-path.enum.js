const AppPath = {
	ROOT: "/",
	MESSAGES: "/messages",
	MESSAGES_$ID: "/messages/:id",
	USERS: "/users",
	LOGIN: "/login",
	ANY: "*",
};

export { AppPath };
