import { useState } from "hooks/hooks";
import "./style.css";

const EditMessageForm = ({ onClose, onSave, message }) => {
	const [currentText, setCurrentText] = useState(message.text);

	const onChangeData = (e) => {
		const value = e.target.value;
		setCurrentText(value);
	};
	const onEdit = () => {
		if (currentText !== "") {
			onSave({ ...message, text: currentText });
		}
	};

	return (
		<>
			<div className="edit-message-modal modal-shown">
				<div className="edit-message-body">
					<textarea
						className="edit-message-input"
						onChange={onChangeData}
						value={currentText}
					></textarea>
					<div className="edit-message-bottom">
						<button className="edit-message-close" onClick={onClose}>
							CLOSE
						</button>
						<button className="edit-message-button" onClick={onEdit}>
							SAVE
						</button>
					</div>
				</div>
			</div>
		</>
	);
};

export default EditMessageForm;
