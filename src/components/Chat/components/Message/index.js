import { useState } from "hooks/hooks";
import "./style.css";
import { convertTime } from "helpers/dateHelper";
const Message = ({ message }) => {
	const [isLike, setIsLike] = useState(false);

	const like = () => {
		setIsLike(!isLike);
	};

	const likeClass = isLike
		? "fas fa-heart message-liked"
		: "far fa-heart message-like";
	return (
		<div className="message">
			<img
				src={message.avatar}
				alt={message.user}
				className="message-user-avatar"
			/>
			<div className="message-body">
				<div>
					<p className="message-user-name">{message.user}</p>
					<p className="message-text">{message.text}</p>
				</div>
				<div className="message-time">{convertTime(message.createdAt)}</div>
				<i className={likeClass} onClick={like}></i>
			</div>
		</div>
	);
};
export default Message;
