import { useState } from "hooks/hooks";
import "./style.css";
const MessageInput = ({ myId, onMessageAdd }) => {
	const [currentText, setCurrentText] = useState("");

	const onChange = (e) => {
		const newTest = e.target.value;
		setCurrentText(newTest);
	};
	const handleMessageAdd = () => {
		if (currentText !== "") {
			onMessageAdd({
				text: currentText,
				createdAt: new Date().toISOString(),
				userId: myId,
			});
			setCurrentText("");
		}
	};
	return (
		<div className="message-input">
			<textarea
				className="message-input-text"
				type="text"
				placeholder="Type your message"
				onChange={(e) => onChange(e)}
				value={currentText}
			/>
			<button className="message-input-button" onClick={handleMessageAdd}>
				SEND
			</button>
		</div>
	);
};

export default MessageInput;
