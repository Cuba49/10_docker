import "./style.css";
import Message from "../Message";
import OwnMessage from "../OwnMessage";
import MessagesDivider from "../MessagesDivider";
import { convertToEngDate } from "helpers/dateHelper";

const MessageList = ({ messages, myId, onMessageDelete, onMessageEdit }) => {
	const messagesBlocks = [];
	let contextDate = null;
	messages.forEach((message) => {
		if (
			convertToEngDate(message.createdAt) !== contextDate &&
			contextDate !== null
		) {
			messagesBlocks.unshift(
				<MessagesDivider date={contextDate} key={contextDate} />
			);
		}
		contextDate = convertToEngDate(message.createdAt);
		messagesBlocks.unshift(
			message.userId === myId ? (
				<OwnMessage
					key={message.id}
					message={message}
					onDelete={onMessageDelete}
					onEdit={onMessageEdit}
				/>
			) : (
				<Message key={message.id} message={message} />
			)
		);
	});
	if (contextDate !== null) {
		messagesBlocks.unshift(
			<MessagesDivider date={contextDate} key={contextDate} />
		);
	}
	return (
		<div className="message-list" id="message-list">
			<div className="message-list-scroll">{messagesBlocks}</div>
		</div>
	);
};
export default MessageList;
