import React from "react";
import "./style.css";
import { convertTime } from "helpers/dateHelper";
const OwnMessage = ({ message, onEdit, onDelete }) => {
	return (
		<div className="own-message">
			<div className="message-body message-body-my">
				<p className="message-text">{message.text}</p>
				<div className="message-time">{convertTime(message.createdAt)}</div>
			</div>
			<i
				className="fas fa-edit message-edit"
				onClick={() => onEdit(message)}
			></i>
			<i
				className="far fa-trash-alt message-delete"
				onClick={() => onDelete(message)}
			></i>
		</div>
	);
};
export default OwnMessage;
