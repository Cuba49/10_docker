import {
	useCallback,
	useEffect,
	useState,
	useDispatch,
	useSelector,
} from "hooks/hooks";
import { messages as messagesActionCreator } from "store/actions";
import "./style.css";
import Header from "./components/Header";
import MessageInput from "./components/MessageInput";
import MessageList from "./components/MessageList";
import Preloader from "../Preloader";
import EditMessageForm from "./components/EditMessageForm";
import { DataStatus } from "common/enums/enums";
import { AppPath } from "common/enums/enums";
import { Redirect } from "react-router";

const Chat = () => {
	const myId = "9e243930-83c9-11e9-8e0c-8f1a686f4ce4";

	const { messages, status, loginUser } = useSelector(
		({ messages, users }) => ({
			messages: messages.messages,
			status: messages.status,
			loginUser: users.loginUser,
		})
	);

	const [currentMessage, setCurrentMessage] = useState(null);
	const dispatch = useDispatch();

	const handleAddPopupClose = () => setCurrentMessage(null);
	const handleMessageEdit = (message) => setCurrentMessage(message);

	const handleMessageSave = useCallback(
		(message) => {
			dispatch(messagesActionCreator.updateMessage(message));
			setCurrentMessage(null);
		},
		[dispatch]
	);
	const handleMessageDelete = useCallback(
		(message) => {
			dispatch(messagesActionCreator.deleteMessage(message));
		},
		[dispatch]
	);
	const handleMessageAdd = useCallback(
		(message) => {
			dispatch(messagesActionCreator.addMessage(message));
		},
		[dispatch]
	);
	const onKeyPress = (e) => {
		if (e.key === "ArrowUp") {
			const lastMessage = messages.find(
				(message) => message.userId === myId
			);
			setCurrentMessage(lastMessage);
		}
	};
	document.addEventListener("keydown", onKeyPress);
	useEffect(() => {
		dispatch(messagesActionCreator.fetchMessages());
	}, [dispatch]);

	const getCountUsers = () => {
		const uniqueUsers = [];
		messages.forEach((message) => {
			const userId = message.userId;
			if (uniqueUsers.indexOf(userId) === -1) {
				uniqueUsers.push(userId);
			}
		});
		return uniqueUsers.length;
	};

	const getLastMessageAt = () => {
		const sortMessages = messages;
		return sortMessages.length > 0 ? sortMessages[0].createdAt : null;
	};
	if (!loginUser?.id) {
		return <Redirect to={AppPath.LOGIN} />;
	}
	if (status === DataStatus.PENDING) {
		return <Preloader />;
	}
	return (
		<div className="chat">
			<Header
				name="Вселенский заговор"
				countUsers={getCountUsers()}
				countMessages={messages.length}
				lastMessageAt={getLastMessageAt()}
			/>
			<MessageList
				messages={messages}
				myId={myId}
				onMessageDelete={handleMessageDelete}
				onMessageEdit={handleMessageEdit}
			/>
			<MessageInput myId={myId} onMessageAdd={handleMessageAdd} />
			{currentMessage && (
				<EditMessageForm
					onClose={handleAddPopupClose}
					onSave={handleMessageSave}
					message={currentMessage}
				/>
			)}
		</div>
	);
};

export default Chat;
