import "./style.css";
import { useState, useSelector, useCallback, useDispatch } from "hooks/hooks";
import { users as usersActionCreator } from "store/actions";
import { Redirect } from "react-router-dom";
import { AppPath } from "common/enums/enums";
const Login = () => {
	const { loginUser } = useSelector(({ users }) => ({
		loginUser: users.loginUser,
	}));

	const dispatch = useDispatch();
	const [currentUser, setCurrentUser] = useState({ name: "", password: "" });
	const handleChange = ({ target }) => {
		let { name, value } = target;
		setCurrentUser({ ...currentUser, [name]: value });
	};
	const onLogin = useCallback(
		(user) => {
			dispatch(usersActionCreator.login(user));
		},
		[dispatch]
	);
	if (loginUser?.isAdmin === true) {
		return <Redirect to={AppPath.USERS} />;
	} else if (loginUser?.id) {
		return <Redirect to={AppPath.ROOT} />;
	}
	return (
		<div className="login">
			<div className="login-body">
				<input
					placeholder="Login"
					name="name"
					onChange={handleChange}
					value={currentUser.name}
				></input>
				<input
					placeholder="Password"
					name="password"
					onChange={handleChange}
					value={currentUser.password}
				></input>
				<button onClick={() => onLogin(currentUser)}>LOGIN</button>
			</div>
		</div>
	);
};
export default Login;
