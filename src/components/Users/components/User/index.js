import "./style.css";
const User = ({ user, onUserDelete, onUserEdit }) => {
	return (
		<div className="user">
			<p className="user-name">{user.name}</p>
			<p className="user-mail">{user.mail}</p>
			<p className="user-isAdmin">{user.isAdmin ? "Admin" : ""}</p>
			<button className="button" onClick={() => onUserEdit(user)}>
				EDIT
			</button>
			<button
				className="button button-delete"
				onClick={() => onUserDelete(user)}
			>
				DELETE
			</button>
		</div>
	);
};
export default User;
