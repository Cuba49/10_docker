import "./style.css";
import User from "../User";
const UserList = ({ users, myId, onUserDelete, onUserEdit }) => {
	return (
		<div className="user-list">
			{users.map((user) => (
				<User
					key={user.id}
					user={user}
					onUserDelete={onUserDelete}
					onUserEdit={onUserEdit}
				/>
			))}
		</div>
	);
};
export default UserList;
