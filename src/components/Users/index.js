import {
	useCallback,
	useEffect,
	useState,
	useDispatch,
	useSelector,
} from "hooks/hooks";
import { users as usersActionCreator } from "store/actions";
import "./style.css";
import Preloader from "../Preloader";
import { DataStatus } from "common/enums/enums";
import UserList from "./components/UserList";
import EditUserForm from "./components/EditUserForm";
import { Link } from "react-router-dom";
import { AppPath } from "common/enums/enums";
import { Redirect } from "react-router";

const Users = () => {
	const myId = "9e243930-83c9-11e9-8e0c-8f1a686f4ce4";

	const { users, status, loginUser } = useSelector(({ users }) => ({
		users: users.users,
		status: users.status,
		loginUser: users.loginUser,
	}));
	const EMPTY_USER = { name: "", mail: "", isAdmin: false, password: "" };
	const [currentUser, setCurrentUser] = useState(null);
	const dispatch = useDispatch();
	const handleAddPopupOpen = () => setCurrentUser(EMPTY_USER);
	const handleAddPopupClose = () => setCurrentUser(null);
	const handleUserEdit = (user) => setCurrentUser(user);

	const handleUserSave = useCallback(
		(user) => {
			const isUpdate = Boolean(user.id);
			dispatch(
				isUpdate
					? usersActionCreator.updateUser(user)
					: usersActionCreator.addUser(user)
			);
			setCurrentUser(null);
		},
		[dispatch]
	);
	const handleUserDelete = useCallback(
		(user) => {
			dispatch(usersActionCreator.deleteUser(user));
		},
		[dispatch]
	);
	useEffect(() => {
		dispatch(usersActionCreator.fetchUsers());
	}, [dispatch]);
	if (loginUser?.isAdmin !== true) {
		return <Redirect to={AppPath.ROOT} />;
	}
	if (status === DataStatus.PENDING) {
		return <Preloader />;
	}
	return (
		<div className="users">
			<div className="users-navigation">
				<Link to={AppPath.ROOT} className="users-navigation-chat">
					Back to Chat
				</Link>
				<button
					className="users-navigation-create"
					onClick={handleAddPopupOpen}
				>
					Add User
				</button>
			</div>
			<UserList
				users={users}
				myId={myId}
				onUserDelete={handleUserDelete}
				onUserEdit={handleUserEdit}
			/>
			{currentUser && (
				<EditUserForm
					onClose={handleAddPopupClose}
					onSave={handleUserSave}
					user={currentUser}
				/>
			)}
		</div>
	);
};

export default Users;
