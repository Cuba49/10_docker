import { createAsyncThunk } from "@reduxjs/toolkit";
import { ActionType } from "./common";

const fetchMessage = createAsyncThunk(
	ActionType.FETCH_MESSAGE,
	async (id, { extra }) => ({
		todo: await extra.messagesService.getOne(id),
	})
);

export { fetchMessage };
