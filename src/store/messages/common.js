const ActionType = {
	FETCH_MESSAGES: "messages/fetch-messages",
	ADD: "messages/add",
	UPDATE: "messages/update",
	HARD_UPDATE: "messages/hard-update",
	DELETE: "messages/delete",
};

export { ActionType };
